import 'package:flutter/material.dart';
import 'package:promotion_product_hydra/page/LogIn.dart';
/// AUTHOR: UsRex
// TODO: app para promociones de productos con sqflite
/// 

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'AppVentor',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: LogInPage(),
    );
  }
}
