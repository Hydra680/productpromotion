import 'package:flutter/material.dart';
import 'package:promotion_product_hydra/Helper/HelperAmigos.dart';
import 'package:promotion_product_hydra/Helper/HelperUser.dart';
import 'package:promotion_product_hydra/Models/UserModel.dart';

class LogInPage extends StatefulWidget {
  @override
  _LogInPageState createState() => _LogInPageState();
}

class _LogInPageState extends State<LogInPage> {
  bool swObscure = true;
  bool swEmail = true;
  HelperUser db = HelperUser();
  HelperAmigos amigdb= HelperAmigos();
  
  var controllerEmail = TextEditingController();
  var controllerPass = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      appBar: AppBar(
        elevation: 0,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(50),
              topRight: Radius.circular(50),
            ),
            color: Colors.white),
        child: SingleChildScrollView(
          physics: ScrollPhysics(
              parent: MediaQuery.of(context).orientation == Orientation.portrait
                  ? NeverScrollableScrollPhysics()
                  : AlwaysScrollableScrollPhysics()),
          child: Container(
            padding: EdgeInsets.symmetric(
                    horizontal: 128,
                    vertical: MediaQuery.of(context).size.height) /
                7,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Iniciar Sessión",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontFamily: "arial",
                      fontSize: 32),
                ),
                SizedBox(
                  height: 32,
                ),
                TextFormField(
                  controller: controllerEmail,
                  onChanged: (email) {
                    setState(() {
                      swEmail = _comparateCorreo(email);
                      print(swEmail);
                    });
                  },
                  keyboardType: TextInputType.emailAddress,
                  maxLines: 1,
                  maxLength: 16,
                  decoration: InputDecoration(
                      errorText: swEmail ? null : "email incorrecto",
                      prefixIcon: Icon(Icons.mail),
                      border: UnderlineInputBorder(),
                      labelText: "correo/usuario",
                      helperText: "Coloca tu correo ej. example@example.com"),
                ),
                SizedBox(
                  height: 16,
                ),
                TextFormField(
                  controller: controllerPass,
                  keyboardType: TextInputType.visiblePassword,
                  obscureText: swObscure,
                  maxLines: 1,
                  maxLength: 16,
                  decoration: InputDecoration(
                      prefixIcon: IconButton(
                        icon: Icon(swObscure ? Icons.lock : Icons.lock_open),
                        onPressed: () {
                          setState(() {
                            swObscure = !swObscure;
                          });
                        },
                      ),
                      border: UnderlineInputBorder(),
                      labelText: "contraseña"),
                ),
                _register(),
                RaisedButton(
                  color: Colors.blueGrey,
                  onPressed: () async{
                    // ! query existe usuario
                    if( await db.existUser(controllerEmail.text.toString(), controllerPass.text.toString())){
                      print("exitoso");
                    }else{
                      print("error ...");
                    }
                  },
                  child: Text(
                    "Ingresar",
                    style: TextStyle(color: Colors.white),
                  ),
                  shape: StadiumBorder(),
                  padding: EdgeInsets.symmetric(horizontal: 64, vertical: 6),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  // TODO: Widgets 
  Widget _register() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text("No tienes una cuenta ?"),
        FlatButton(
          onPressed: () {},
          child: Text("Registrarse"),
        ),
      ],
    );
  }
  // TODO: Controladores 
  bool _comparateCorreo(String correo) {
    //String correo = "ho.l@aholacom";
    int end = correo.length - 1;
    String validar = "@.";
    int i = 0;
    for (int x = 0; x < end; x++) {
      if (correo[x] == validar[i] && i < 2) {
        //print(correo[x]);
        //print(validar[i]);
        i = i + 1;
        if (i == 2) {
          print("correcto");
          return true;
        }
      }
    }
    return false;
  }
}
