
import 'package:sqflite/sqflite.dart';

class HelperAmigos {

  static const TABLE_AMIGOS='amigos';

  Future<Database> db = openDatabase(
    'dbUser.db', //nombre de la base de datos
    version: 1,
    onCreate: (Database db, int version) async {
      await db.execute("""
            CREATE TABLE if not exists $TABLE_AMIGOS(
            id_amig INTEGER PRIMARY KEY AUTOINCREMENT,
            ci_a INTEGER,
            ci_b INTEGER,
            foreign key(ci_a) references persona(ci),
            foreign key(ci_b) references persona(ci),
          );""");
    },
  );
  
}