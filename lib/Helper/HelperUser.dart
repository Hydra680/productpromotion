import 'package:promotion_product_hydra/Models/PersonaModel.dart';
import 'package:promotion_product_hydra/Models/UserModel.dart';
import 'package:sqflite/sqflite.dart';

class HelperUser {
  static const TABLE_USER_NAME = 'usuario';
  static const TABLE_PERSONA ='persona';
  

  Future<Database> db = openDatabase(
    'dbUser.db', //nombre de la base de datos
    version: 1,
    onCreate: (Database db, int version) async {
      await db.execute("""
            CREATE TABLE if not exists $TABLE_USER_NAME(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            nickname TEXT,
            email TEXT,
            password TEXT
          );
            create table if not exists $TABLE_PERSONA(
            ci integer primary key autoincrement,
            id integer,
            nombre text not null,
            apellido_pat text not null,
            apellido_mat text not null,
            f_nacimiento text not null,
            foreign key(id) references $TABLE_USER_NAME(id)
          );""");
    },
  );

  Future<bool> existUser(String email, String pass) async{
    Database db = await this.db;
    List<Map<String, dynamic>> respuesta;
    List<UserModel>listaUser;
    try {
      respuesta = await db.query(TABLE_USER_NAME,where: "email = ? and password = ?",whereArgs: [email,pass]);
      listaUser = respuesta.map((Map<String, dynamic> map) {
        return UserModel.fromMap(map);
      }).toList();
    } catch (e) {
      print(e);
    }

    print(listaUser);
    print("************************************");
    print("               READ                 ");
    print("************************************");
    return listaUser.length >0 ? true: false;
  }
  // TODO: CRUD PERSONA
  
  Future<int> insertPersona(PersonaModel personaModel)async{
    Database db = await this.db;
    int respu =await db.insert(TABLE_PERSONA, personaModel.toMap());
    print(">>>> Insert Persona $respu");
    return respu;
  }

  Future<List<PersonaModel>>  readPersona()async{
    Database db = await this.db;
    
    List<Map<String, dynamic>> listaRes = await db.query(TABLE_PERSONA);
    List<PersonaModel> list =listaRes.map((Map<String, dynamic> data){
      return PersonaModel.fromMap(data);
    }).toList();
    print(">>>> Read Persona");
    return list.isEmpty? []: list;
  }

  // TODO: CRUD USUARIO

  Future<List<UserModel>> readUser() async {
    Database db = await this.db;
    List<Map<String, dynamic>> respuesta;
    var listaUser;
    try {
      respuesta = await db.query(TABLE_USER_NAME);
      listaUser = respuesta.map((Map<String, dynamic> map) {
        return UserModel.fromMap(map);
      }).toList();
    } catch (e) {
      print(e);
    }

    print("************************************");
    print("               READ                 ");
    print("************************************");
    return listaUser.isEmpty ? [] : listaUser;
  }

  Future<int> insertUser(UserModel userModel) async {
    Database db = await this.db;
    int respuesta = await db.insert(TABLE_USER_NAME, userModel.toMap());

    print("************************************");
    print("               Insert               ");
    print("************************************");
    return respuesta;
  }

  Future deleteUser(int id) async {
    Database db = await this.db;
    int res =
        await db.delete(TABLE_USER_NAME, where: "id = ?", whereArgs: [id]);

    print("************************************");
    print("           DELETE $res              ");
    print("************************************");
  }

  Future updateUser(UserModel userModel) async {
    Database db = await this.db;
    int res = await db.update(TABLE_USER_NAME, userModel.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
    print("************************************");
    print("           UPDATE $res              ");
    print("************************************");
  }
}
