class PersonaModel{
  int _ci;
  int _id;
  String _nombre;
  String _apellidoPat;
  String _apellidoMat;
  String _fNacimiento;
  
  PersonaModel.fromMap(Map<String, dynamic> map){
    this._ci = map['ci'];
    this._id = map['id'];
    this._apellidoPat = map['apellido_pat'];
    this._apellidoMat = map['apellido_mat'];
    this._fNacimiento = map['f_nacimiento'];
  }

  Map<String, dynamic> toMap(){
    return {
      'ci':this._ci,
      'id': this._id,
      'nombre': this._nombre,
      'apellido_pat': this._apellidoPat,
      'apellido_mat': this._apellidoMat,
      'f_nacimiento': this._fNacimiento
    };
  }



}